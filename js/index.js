window.onload = function () {
    new Vue({
        el: '#con',
        data: {
            username: 'username',
            tabIndex: 0,  // tab
            tabList: [
                { id: 1, name: '标签一' },
                { id: 2, name: '标签二' },

            ],
            itemIndex: 0,  // item
            itemList: [
                { id: 1, name: '类目一' },
                { id: 2, name: '类目二' },
                { id: 3, name: '类目三' },
                { id: 4, name: '类目四' },
            ],
            cardList: [
                { id: 1, icon: 'http://qstatic.zuimeia.com/img/icons/cld/2020012022513482864_690x690.png', title: '团子相机', info: '终于在春节前找到了这款最适合拍全家福的 APP', like: 11, time: '2020-02-02', img: 'http://qstatic.zuimeia.com/img/covers/cld/2020012022513455714_1200x700.png', user: '小闰', from: 'admin', comment: '​每次推荐 iOS 或者 Android 平台的应用，都免不了有小伙伴会可惜，没有自己使用的平台，这个确实是没有办法，很多开发者都是只开发单平台的，只...', },
                // {id: 1, icon: 'http://qstatic.zuimeia.com/img/icons/cld/2020012022513482864_690x690.png', title: '团子相机', info: '终于在春节前找到了这款最适合拍全家福的 APP', like: 11, time: '2020-02-02', img: 'http://qstatic.zuimeia.com/img/covers/cld/2020012022513455714_1200x700.png', user: '小闰', from: 'admin', comment: '​每次推荐 iOS 或者 Android 平台的应用，都免不了有小伙伴会可惜，没有自己使用的平台，这个确实是没有办法，很多开发者都是只开发单平台的，只...',},
            ],
            courseList_800: [
                { id: 1, name: '课程一' },
                { id: 1, name: '课程一' },
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
            ],
            courseList_1200: [
                { id: 1, name: '课程一' },
                { id: 1, name: '课程一' },
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
                // {id: 1, name: '课程一'},
            ],
            tagObj: {
                tagName: "标签名称",
                tagList: [
                    { id: 1, name: '标签一' },
                    { id: 1, name: '标签一' },
                    { id: 1, name: '标签一' },
                    { id: 1, name: '标签一' },
                    { id: 1, name: '标签一' },
                ]
            },


        },
        created: function () {

        },
        mounted: function () {

        },
        methods: {
            switchTab: function (e) {
                this.tabIndex = e;
            },
            switchItem: function (e) {
                this.itemIndex = e;
            },
        }
    })
}